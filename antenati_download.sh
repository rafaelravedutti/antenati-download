#!/bin/bash

# Antenati base URL
ANTENATI_BASE="http://dl.antenati.san.beniculturali.it"

# Province
#PROVINCE="Avellino"
#PROVINCE="Benevento"
PROVINCE="Caserta"
#PROVINCE="Salerno"

# Registry type
ARCHIVE="Stato+civile+napoleonico+e+della+restaurazione"
#ARCHIVE="Stato+civile+della+restaurazione"
#ARCHIVE="Stato+civile+italiano"

# Antenati full base URL
ANTENATI_FULL_BASE="${ANTENATI_BASE}/v/Archivio+di+Stato+di+${PROVINCE}/${ARCHIVE}"

# Comuni to download registries
COMUNI="Arienzo"

# Record types
RECORD_TYPES="Nati"
#RECORD_TYPES="Matrimoni"
 
# Year range
YEARS=$(seq 1855 1858)
#YEARS=$(seq 1865 1879)

# Download only first ${HEAD} images (blank to download all)
# Useful for records with indexes
HEAD=

# Download only last ${TAIL} images (blank to download all)
# Useful for records with indexes
TAIL=

# Sleep time (avoid flooding)
SLEEP_TIME=2

# Go through the specified comuni
for comune in ${COMUNI}; do
  # Go through the specified record types
  for record_type in ${RECORD_TYPES}; do
    # Go through all the years in the specified range
    for year in ${YEARS}; do
      # Download directory
      download_dir="${comune}/${record_type}/${year}"

      # Base URL for the current year
      base_url="${ANTENATI_FULL_BASE}/${comune}/${record_type}/${year}"

      # Get the URLs for the current year, it is only considered the
      # URLs within the current page and the main-menu URL is removed
      # to avoid duplicates
      year_urls=$(
        lynx -listonly -dump -nonumbers "${base_url}" |
        grep "${base_url}" |
        grep -v "main-menu"
      )

      # Go through the obtained URLs
      for yurl in ${year_urls}; do
        # Initialize image array
        declare -a image_array

        # Image counter
        counter=1

        # Get the prefix for the current URL, it is the last part
        # specified in the URL
        yurl_prefix="${yurl::-1}"
        yurl_prefix="${yurl_prefix##*/}"

        echo "Looking through ${yurl} (prefix = ${yurl_prefix})"

        # Next URL is first defined as the current URL
        next_url=${yurl}
        # Next page to look is set to two by default
        next_page=2

        # Keep running flag is set to true by default
        keep_alive=true

        # While we should keep running and there is a next URL
        # defined for us to keep looking
        while [ "$keep_alive" = true -a -n "${next_url}" ]; do
          echo "Current url: ${next_url}"

          # Get the images links in the current page
          images=$(
            lynx -listonly -dump -nonumbers ${next_url} |
            grep ".jpg.html" |
            uniq
          )

          # Get the URL for the next page in the current page
          next_url=$(
            lynx -listonly -dump -nonumbers ${next_url} |
            grep "?g2_page=${next_page}&" |
            uniq
          )

          # Set keep alive flag as false in case we do not find
          # any image in the current page
          keep_alive=false

          # Go through all the images
          for img in ${images}; do
            echo "Retrieving ${img}..."

            # Add image download link into the array
            image_array[$counter]=$img

            # Increase image counter
            counter=$((counter + 1))

            # Set keep alive flag
            keep_alive=true
          done

          # Increase next page to look for
          next_page=$((next_page + 1))
        done

        # Range of indexes to download images
        range_start=1
        range_end=$((counter - 1))

        if [ -n "${HEAD}" ]; then
          range_end=$((range_start + HEAD - 1))
        fi

        if [ -n "${TAIL}" ]; then
          range_start=$((range_end - TAIL + 1))
        fi

        # Start of range must be greater or equal than zero
        if [ "$range_start" -lt 1 ]; then
          range_start=1
        fi

        # End of range must be less or equal than the last array element
        if [ "$range_end" -gt $((counter - 1)) ]; then
          range_end=$((counter - 1))
        fi

        # Check if range is valid
        if [ "$range_start" -le "$range_end" ]; then
          # Finally, download the images
          for idx in $(seq $range_start $range_end); do
            # Get the image source link
            img_src=$(
              lynx -listonly -dump -nonumbers "${image_array[$idx]}" |
              grep "DownloadItem" |
              uniq
            )

            # Create the subdirectory with the obtained prefix
            # so we can separate each year url in the files
            mkdir -p "${download_dir}/${yurl_prefix}"

            echo "Downloading ${img_src}..."
            lynx -dump ${img_src} > ${download_dir}/${yurl_prefix}/${idx}.jpeg

            # Sleep for sometime to avoid flooding
            sleep ${SLEEP_TIME}
          done
        fi
      done
    done
  done
done
